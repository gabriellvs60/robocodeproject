package fatec2018;
import robocode.*;
import java.awt.*;
import robocode.util.Utils;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * Deadpool - a robot by Group 4
 */
public class Deadpool extends TeamRobot
{
	/**
	 * run: Deadpool's default behavior
	 */
	public void run() {
		//Insere as cores vermelha, preta, branca, branca e amarela respectivamente para o corpo, arma, radar, scan e o tiro do robo.
		setBodyColor(Color.red);
		setGunColor(Color.black);
		setRadarColor(Color.white);
		setScanColor(Color.white);
		setBulletColor(Color.yellow);
		
		// variavel booleana para qual direção está.
		boolean direcao = true;
		setAdjustGunForRobotTurn(true); // Quando o robô gira, ajusta a torreta na direção oposta
		setAdjustRadarForGunTurn(true); // Quando a torreta gira, ajusta o radar na diração oposta
		while(true) {
			// primeiramente testa se a posição esta perto da parede invisivel
 // gira a esquerda o radas, de forma continua
			onHitInvisibleWall();
            turnRadarLeftRadians(1);
//			turnGunRight(360);
			// gira para direita ou esquerda dependendo da variavel direcao ( true = direita, false = esquerda).
			if(direcao){
				turnRight(80);
				direcao = false;
			}else{
				turnLeft(80);
				direcao = true;
			}
			// anda 100 pixels para frente na direção passada anteriormente.
			ahead(100);
            turnRadarLeftRadians(1); // gira a esquerda o radas, de forma continua
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		// faz o teste se o robo scaneado é aliado.	
		if (isTeammate(e.getName())) {
			return;
		}
		// faz um teste se o nome do robo escaneado é o BorderGuard.
		if(!e.getName().equals("samplesentry.BorderGuard")){

            setTurnGunRightRadians(Utils.normalRelativeAngle((getHeadingRadians() + e
                    .getBearingRadians()) - getGunHeadingRadians())); // move a torreta na direção dele
				if(e.getDistance() < 30){ // se está a menos de 200 pixels de distancia
                setFire(8.5); // mete bala nele, nivel 3
            } 
			if(e.getDistance() < 100){ // se está a menos de 200 pixels de distancia
                setFire(5.5); // mete bala nele, nivel 3
            }
            if(e.getDistance() < 200){ // se está a menos de 200 pixels de distancia
                setFire(3); // mete bala nele, nivel 3
            }
			 if(e.getDistance() > 750 && e.getEnergy() > 20){ // se está a mais de 750 de distancia, e o alvo ta vivo, poupa energia
                //não atira
            }
            else{
                setFire(1); // senão atira com 2.4
            }
//
            double radarTurn = getHeadingRadians() + e.getBearingRadians()
                    - getRadarHeadingRadians();
            setTurnRadarRightRadians(2 * Utils.normalRelativeAngle(radarTurn)); // lock radar
			/* se a distancia for menor que 150 pixels irá atirar com fogo 3.
			if(e.getDistance() < 150){
				fire(3);
			// se não, ira atirar com fogo 1.
			}else{
				fire(1);
			}
			*/
		}
    }

	//quando for atingido por um tiro, irá girar para direita 90º e andar para frente 100 pixels.
	public void onHitByBullet(HitByBulletEvent e) {
		onHitInvisibleWall();
		turnRight(90);
		ahead(150);
	}
	
	// metodo para testar se a posição do robo está 20% em torno da arena.
	public void onHitInvisibleWall() {
		if(getX() > (getBattleFieldWidth() -(getBattleFieldWidth() * 0.2))){
			System.out.println("entrou1");
			if(getHeading() < 270 && getHeading() >= 90){
				System.out.println("entrou1.2\n " + getHeading());
				turnRight(270 - getHeading());
				ahead(150);
			}else if(getHeading() > 270 && getHeading() <= 359){
				System.out.println("entrou1.1\n " + getHeading());
				turnLeft(getHeading() - 270);
				ahead(150);
			}else if(getHeading() < 90 && getHeading() >= 0){
				System.out.println("entrou1.3\n " + getHeading());
				turnRight(270 - getHeading());
				ahead(150);
			}
		}else if((getY() > (getBattleFieldHeight() - (getBattleFieldHeight() * 0.2)))){
		System.out.println("entrou2");
			if(getHeading() > 180 || getHeading() < 359){
				System.out.println("entrou2.1");
				turnLeft(getHeading() - 180);
				ahead(150);
			}else if(getHeading() < 180 || getHeading() >= 0){
		System.out.println("entrou2.2");
				turnRight(180 - getHeading());
				ahead(150);
			}
		}else if((getX() < (getBattleFieldWidth() - (getBattleFieldWidth() * 0.80)))){
		System.out.println("entrou3");
			if(getHeading() > 270 && getHeading() <= 359){
				System.out.println("entrou3.1");
				turnRight(getHeading() - 90);
				ahead(150);
			}else if(getHeading() < 90 && getHeading() >= 0){
				System.out.println("entrou3.3");
				turnRight(90 - getHeading());
				ahead(150);
			}else if(getHeading() < 270 || getHeading() >= 90){
				System.out.println("entrou3.2");
				turnLeft(getHeading() - 90);
				ahead(150);
			}
		}else if((getY() < (getBattleFieldHeight() - (getBattleFieldHeight() * 0.80)))){
		System.out.println("entrou4");
			if(getHeading() > 180 || getHeading() <= 359){
				System.out.println("entrou4.1");
				turnRight(360 - getHeading());
				ahead(150);
			}else if(getHeading() < 180 || getHeading()>= 0){
				System.out.println("entrou4.2");
				turnLeft(getHeading());
				ahead(150);
			}
		}
	}

	// qando atingir outro robo, irá girar 60º e andar 100 pixels a frente.
	public void onHitRobot(HitRobotEvent e){
		onHitInvisibleWall();
		turnLeft(90);
		ahead(100);	
	}
}