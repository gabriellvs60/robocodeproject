package fatec2018;
import robocode.*;

import java.awt.Color;
//import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;

//import java.awt.Color;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * Bug - a robot by (your name here)
 */
public class Bug extends AdvancedRobot
{

private int campoBatalha = 450;
//declaração de variáveis...
private boolean moveu = false; //se precisa andar ou girar
private boolean noCanto = false; //se o robô está num canto
private String alvo;

private byte dir = 1; //direção ao mover
private short antigaEnergia; // energia anterior do robô alvo.
	/**
	 * run: Bug's default behavior
	 */
	public void run() {
	

public void onScannedRobot(ScannedRobotEvent e){
	
	// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		// setColors(Color.red,Color.blue,Color.green); // body,gun,radar

		// Robot main loop
		while(true) {
		
if(getDistanceRemaining() == 0 && getTurnRemaining() == 0){ //não está andando ou girando
            if(noCanto){
                if(moveu){ 
                    setTurnLeft(90); //gira este ciclo
                    moveu = false; 
                }
                else{ // senão
                    setAhead(160 * dir); //move neste ciclo
                    moveu = true; 
                }
            }
               else{
              
				//se o angulo diferente de 0º~89º vai entrar no if
                if((getHeading() % 90) != 0){
				// se o Y for maior que a metade do campo, vai virar para esquerda pegando o angulo que esta, se não, vai virar para esquerda pegando o angulo que esta - 180º
                    setTurnLeft((getY() > (campoBatalha / 2)) ? getHeading()
                            : getHeading() - 180);
                }
                //se não estamos em cima ou embaixo, vai pra região mais próxima
				//se não se o Y for maior que 30 pixels e Y for menor que o campo de batalha - 30 pixels
                else if(getY() > 30 && getY() < campoBatalha - 30){
					//se o angulo atual for maior que 90º, vai pegar o Y - 20, se não, campo de batalha - Y pixels - 20 pixels
                    setAhead(getHeading() > 90 ? getY() - 20 : campoBatalha - getY()
                            - 20);
                }
                // Caso não esteja olhando para leste ou oeste vira pra ele
				// se não se o angulo atual for diferente de 90º e o angulo atual for diferente de 270º
                else if(getHeading() != 90 && getHeading() != 270){
                    // se o X for menor que 350 pixels
					if(getX() < 350){
						// vai virar para esquerda 90 pixels caso o Y for maior que 300 pixels, se não vai virar para esquerda -90 pixels
                        setTurnLeft(getY() > 300 ? 90 : -90);
                    // se o X for maior que 350 pixels
					}else{
						// vai virar para esquerda -90 pixels caso o Y for maior que 300 pixels, se não vai virar para esquerda 90 pixels  
                        setTurnLeft(getY() > 300? -90 : 90);
                    }
                }
                    // Se não está na esquerda ou direita, vai pro mais próximo
					// se não se o X for maior que 30 e X for menor que o campo de batalha - 30 pixels
                else if(getX() > 30 && getX() < campoBatalha - 30){
					// vai para frente X pixels - 20 pixels se o angulo atual for menor que 180º, se não vai para frente campo de batalha - X pixels - 20 pixels
                    setAhead(getHeading() < 180 ? getX() - 20 : campoBatalha - getX()
                            - 20);
                }
                // está no canto, gira e se move
				// se não se o angulo for igual a 270º
                else if(getHeading() == 270){
					//vai virar para esquerda 90º caso Y for maior que 200 pixels, se não vira para esquerda 180º
                    setTurnLeft(getY() > 200 ? 90 : 180);
					// coloca como verdadeiro variavel canto
                    noCanto = true;
                }
                // está no canto, gira e se move
				// se não se o angulo atual for igual a 90º
                else if(getHeading() == 90){
					// vai virar para esquerda 180º caso o Y for maior que 200 pixels, se não virar para esquerda 90º
                    setTurnLeft(getY() > 200 ? 180 : 90);
					// coloca como verdadeiro variavel canto
                    noCanto = true;
                }
            }
		}
	}
}



}


}
