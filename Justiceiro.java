package fatec2018;
import robocode.*;
import java.awt.*;

// API help : http://robocode.sourceforge.net/docs/robocode/robocode/Robot.html

/**
 * Justiceiro - a robot by (your name here)
 */
public class Justiceiro extends TeamRobot
{
	/**
	 * run: Justiceiro's default behavior
	 */
public void run() {
		//Insere as cores:
		setBodyColor(Color.red); // Corpo do robo: vermelho;
		setGunColor(Color.black);// Canhão do robo: preto;
		setRadarColor(Color.white); //Radar do robo: branco; 
		setScanColor(Color.white);  //Scanner do robo: branco;
		setBulletColor(Color.yellow);//Tiro do robo: amarelo;
		
		// variavel booleana para qual direção está.
		boolean direcao = true;
		
		while(true) {
			// primeiramente testa se a posição esta perto da parede invisivel
			onHitInvisibleWall();
			turnGunRight(360);
			// gira para direita ou esquerda dependendo da variavel direcao ( true = direita, false = esquerda).
		   if(direcao){
		   		turnRight(180);
				ahead(100);
				direcao = false;
				turnGunRight(360);
			}else{
				turnLeft(180);
				direcao = true;
				turnGunLeft(180);
			}
			// anda 100 pixels para frente na direção passada anteriormente.
			ahead(150);
			turnGunRight(360);
			back(150);
			ahead(150);
			turnRight(180);
			turnGunRight(270);
			direcao=false;
			ahead(100);
			
			
			
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		// Verifica se o tanque scaneado é amigo, se for atira e continua .	
		if (isTeammate(e.getName())) {
		    scan();
			return;
		}
		// faz um teste se o nome do robo escaneado é o BorderGuard.
		if(!e.getName().equals("samplesentry.BorderGuard")){
				if(e.getDistance() < 15){ // se está a menos de 15 pixels de distancia
                setFire(8.5); // mete bala nele, nivel 8
            } 
			if(e.getDistance() < 100){ // se está a menos de 100 pixels de distancia
                setFire(5.5); // mete bala nele, nivel 5
            }
            if(e.getDistance() < 200){ // se está a menos de 200 pixels de distancia
                setFire(3); // mete bala nele, nivel 3
            }
			 if(e.getDistance() > 750 && e.getEnergy() > 20){ // se está a mais de 750 de distancia, e o alvo ta vivo, poupa energia
                //não atira
            }
            else{
                setFire(1); // senão atira com 2.4
            }
		}
    }

	//quando for atingido por um tiro, irá girar para direita 180º e andar para frente 100 pixels.
	public void onHitByBullet(HitByBulletEvent e) {
		onHitInvisibleWall();
		turnRight(180);
		ahead(100);
	}


	// metodo para testar se a posição do robo está 20% em torno da arena.
	public void onHitInvisibleWall() {
		if(getX() > (getBattleFieldWidth() -(getBattleFieldWidth() * 0.2))){
			System.out.println("entrou1");
			if(getHeading() < 270 && getHeading() >= 90){
				System.out.println("entrou1.2\n " + getHeading());
				turnRight(270 - getHeading());
				ahead(150);
			}else if(getHeading() > 270 && getHeading() <= 359){
				System.out.println("entrou1.1\n " + getHeading());
				turnLeft(getHeading() - 270);
				ahead(150);
			}else if(getHeading() < 90 && getHeading() >= 0){
				System.out.println("entrou1.3\n " + getHeading());
				turnRight(270 - getHeading());
				ahead(150);
			}
		}else if((getY() > (getBattleFieldHeight() - (getBattleFieldHeight() * 0.2)))){
		System.out.println("entrou2");
			if(getHeading() > 180 || getHeading() < 359){
				System.out.println("entrou2.1");
				turnLeft(getHeading() - 180);
				ahead(150);
			}else if(getHeading() < 180 || getHeading() >= 0){
		System.out.println("entrou2.2");
				turnRight(180 - getHeading());
				ahead(150);
			}
		}else if((getX() < (getBattleFieldWidth() - (getBattleFieldWidth() * 0.80)))){
		System.out.println("entrou3");
			if(getHeading() > 270 && getHeading() <= 359){
				System.out.println("entrou3.1");
				turnRight(getHeading() - 90);
				ahead(150);
			}else if(getHeading() < 90 && getHeading() >= 0){
				System.out.println("entrou3.3");
				turnRight(90 - getHeading());
				ahead(150);
			}else if(getHeading() < 270 || getHeading() >= 90){
				System.out.println("entrou3.2");
				turnLeft(getHeading() - 90);
				ahead(150);
			}
		}else if((getY() < (getBattleFieldHeight() - (getBattleFieldHeight() * 0.80)))){
		System.out.println("entrou4");
			if(getHeading() > 180 || getHeading() <= 359){
				System.out.println("entrou4.1");
				turnRight(360 - getHeading());
				ahead(150);
			}else if(getHeading() < 180 || getHeading()>= 0){
				System.out.println("entrou4.2");
				turnLeft(getHeading());
				ahead(150);
			}
		}
	}

	//Ao atingir outro robo, irá girar 60º à esquerda e retroceder 100 pixels.
	public void onHitRobot(HitRobotEvent e){
		onHitInvisibleWall();
		turnLeft(60);
		back(100);	
	}
}