package fatec2018;

import java.awt.Color;
//import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import robocode.*;

public class Demolidor extends TeamRobot{

//declaração de variáveis...
private boolean moveu = false; //se precisa andar ou girar
private boolean noCanto = false; //se o robô está num canto
private String alvo;
private byte giros = 0; //contador de voltas
private byte dir = 1; //direção ao mover
private short antigaEnergia; // energia anterior do robô alvo.

private int campoBatalha = 450; // serve para limitar a regiao de atuação do tanque

//setando as cores do tanque, respectivamente
 ///setGunColor(Color.black);
	//setBodyColor(Color.red);
	//setRadarColor(Color.white);
	//setBulletColor(Color.white);
	//setScanColor(Color.yellow);
	//	Color bodyColor,
	//Color gunColor,
	//Color radarColor,
	//Color bulletColor,
	//Color scanArcColor


//Método Update
 public void run(){
 		 setColors(Color.RED, Color.BLACK, Color.WHITE, Color.YELLOW, Color.YELLOW); // setA AS CORES
       
        //Define que a torreta terá rotação independente do robô.
        setAdjustGunForRobotTurn(true); // Quando o robô gira, ajusta a torreta na direção oposta
        
        //Define que o radar será independente da rotação da arma
        setAdjustRadarForGunTurn(true); // Quando a torreta gira, ajusta o radar na diração oposta
        while(true){ 
            turnRadarLeftRadians(1); // gira a esquerda o radas, de forma continua
        }
    }
    
         public void onHitByBullet(HitByBulletEvent e){ // se tomou tiro
        alvo = e.getName(); // o alvo é quem atirou
    }
    
     public void onScannedRobot(ScannedRobotEvent e){
	 	// faz o teste se o robo scaneado é aliado.	
		if (isTeammate(e.getName())) {
			return;
		}
         //ignora o BorderGuard
	if(!e.getName().equals("samplesentry.BorderGuard")){
        if(alvo == null || giros > 6){ // se não tem alvo
            alvo = e.getName(); // pega o primeiro robô que aparecer
        }
         
          if(getDistanceRemaining() == 0 && getTurnRemaining() == 0){ //não está andando ou girando
            if(noCanto){
                if(moveu){ 
                    setTurnLeft(90); //gira este ciclo
                    moveu = false; 
                }
                else{ // senão
                    setAhead(160 * dir); //move neste ciclo
                    moveu = true; 
                }
            }
               else{
              
				//se o angulo diferente de 0º~89º vai entrar no if
                if((getHeading() % 90) != 0){
				// se o Y for maior que a metade do campo, vai virar para esquerda pegando o angulo que esta, se não, vai virar para esquerda pegando o angulo que esta - 180º
                    setTurnLeft((getY() > (campoBatalha / 2)) ? getHeading()
                            : getHeading() - 180);
                }
                //se não estamos em cima ou embaixo, vai pra região mais próxima
				//se não se o Y for maior que 30 pixels e Y for menor que o campo de batalha - 30 pixels
                else if(getY() > 30 && getY() < campoBatalha - 30){
					//se o angulo atual for maior que 90º, vai pegar o Y - 20, se não, campo de batalha - Y pixels - 20 pixels
                    setAhead(getHeading() > 90 ? getY() - 20 : campoBatalha - getY()
                            - 20);
                }
                // Caso não esteja olhando para leste ou oeste vira pra ele
				// se não se o angulo atual for diferente de 90º e o angulo atual for diferente de 270º
                else if(getHeading() != 90 && getHeading() != 270){
                    // se o X for menor que 350 pixels
					if(getX() < 350){
						// vai virar para esquerda 90 pixels caso o Y for maior que 300 pixels, se não vai virar para esquerda -90 pixels
                        setTurnLeft(getY() > 300 ? 90 : -90);
                    // se o X for maior que 350 pixels
					}else{
						// vai virar para esquerda -90 pixels caso o Y for maior que 300 pixels, se não vai virar para esquerda 90 pixels  
                        setTurnLeft(getY() > 300? -90 : 90);
                    }
                }
                    // Se não está na esquerda ou direita, vai pro mais próximo
					// se não se o X for maior que 30 e X for menor que o campo de batalha - 30 pixels
                else if(getX() > 30 && getX() < campoBatalha - 30){
					// vai para frente X pixels - 20 pixels se o angulo atual for menor que 180º, se não vai para frente campo de batalha - X pixels - 20 pixels
                    setAhead(getHeading() < 180 ? getX() - 20 : campoBatalha - getX()
                            - 20);
                }
                // está no canto, gira e se move
				// se não se o angulo for igual a 270º
                else if(getHeading() == 270){
					//vai virar para esquerda 90º caso Y for maior que 200 pixels, se não vira para esquerda 180º
                    setTurnLeft(getY() > 200 ? 90 : 180);
					// coloca como verdadeiro variavel canto
                    noCanto = true;
                }
                // está no canto, gira e se move
				// se não se o angulo atual for igual a 90º
                else if(getHeading() == 90){
					// vai virar para esquerda 180º caso o Y for maior que 200 pixels, se não virar para esquerda 90º
                    setTurnLeft(getY() > 200 ? 180 : 90);
					// coloca como verdadeiro variavel canto
                    noCanto = true;
                }
            }
        }

        if(e.getName().equals(alvo)){ // se o robô escaneado é nosso alvo
            giros = 0; // reseta o contador de giros

            

            setTurnGunRightRadians(Utils.normalRelativeAngle((getHeadingRadians() + e
                    .getBearingRadians()) - getGunHeadingRadians())); // move a torreta na direção do alvo
			
 		if(e.getDistance() < 30){ // se está a menos de 30 pixels de distancia
                setFire(8.5); // mete bala nele, nivel 8
            } 
			if(e.getDistance() < 100){ // se está a menos de 100 pixels de distancia
                setFire(5.5); // mete bala nele, nivel 5
            }
            if(e.getDistance() < 200){ // se está a menos de 200 pixels de distancia
                setFire(3); // mete bala nele, nivel 3
            }
			 if(e.getDistance() > 750 && e.getEnergy() > 20){ // se está a mais de 750 de distancia, e o alvo ta vivo, poupa energia
               return;
            }
            else{
                setFire(1); // senão atira com 2.4
            }

            double radarTurn = getHeadingRadians() + e.getBearingRadians()
                    - getRadarHeadingRadians();
                    //normaliza o angulo do alvo de acordo com o angulo do proprio robo
            setTurnRadarRightRadians(2 * Utils.normalRelativeAngle(radarTurn)); // lock radar
        	}else if(alvo != null){ // senão, se o alvo é nulo.
            giros++; // mais um no conta giros
        	}
    	}
	}
}